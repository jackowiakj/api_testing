import requests


def test_default_beers():
    response = requests.get("https://api.punkapi.com/v2/beers")
    body = response.json()
    assert body[0]["id"] == 1
    assert body[-1]['id'] == 25
    assert len(body) == 25


def test_ids_11_to_20():
    """Punk API: assert/v2/beers checks if our results are from 11 to 20"""
    params = {
        'ids': '11|12|13|14|15|16|17|18|19|20'
    }
    response = requests.get('https://api.punkapi.com/v2/beers', params=params)
    body = response.json()
    assert len(body) == 10

    beer_id = 11
    for beer in body:
        beer['id'] == beer_id
        beer_id += 1

def test_id_is_123():
    """Punk API: assert/v2/beers checks if ID=123"""
    response = requests.get("https://api.punkapi.com/v2/beers?ids=123")
    body = response.json()
    assert body[0]["id"] == 123
