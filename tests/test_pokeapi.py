from utils.pokeapi_handler import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


def test_default_list_of_pokemons():
    response = pokeapi_handler.get_list_of_pokemons()
    response_body = response.json()

    # response_is_not_empty
    assert len(response_body["results"]) > 0
    assert response_body["results"]
    assert len(response_body["results"]) != 0
    # assert "results" in response_body -> check only if list exists

    # status_code_is_equal_200
    assert response.status_code == 200

    # numbers_of_pokemon_is_1279
    assert response_body["count"] == 1279

    # time_response_is_under_1s
    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    # size_of_response_is_under_100_kB
    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100


def test_pagination():
    params = {
        'limit': 100,
        'offset': 20
    }
    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()

    assert len(body["results"]) == params['limit']

    splitted = (body['results'][0]['url']).split('/')
    assert int(splitted[-2]) == (params['offset'] + 1)

    assert body['results'][0]['url'] == f'https://pokeapi.co/api/v2/pokemon/{params["offset"] + 1}/'

    assert body['results'][-1]['url'] == f'https://pokeapi.co/api/v2/pokemon/{params["offset"] + params["limit"]}/'


def test_shapes():
    body = pokeapi_handler.get_shapes_of_pokemons().json()

    assert body['count'] == len(body['results'])

    pokemon_shape = body['results'][2]['name']
    body = pokeapi_handler.get_shapes_of_pokemons(pokemon_shape).json()
    assert body['id'] == 3
