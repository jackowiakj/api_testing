from faker import Faker
from utils.gorest_handler import GoRESTHandler

gorest_handler = GoRESTHandler()


def test_create_user():
    """Gorest API: assert/v2/users creates new user, updates and deletes user"""
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    assert 'id' in body
    user_id = body['id']
    body = gorest_handler.get_user(user_id).json()
    assert body['email'] == user_data['email']
    assert body['name'] == user_data['name']
# UPDATE
    user_new_data = {
        "name": Faker().name(),
        "email": Faker().email(),
        "status": "active"
    }
    user_id = body['id']
    body = gorest_handler.update_user(user_id, user_new_data).json()
    assert body['email'] == user_new_data['email']
    assert body['name'] == user_new_data['name']
    print(body)
# DELETE
    gorest_handler.delete_user(user_id)
    body = gorest_handler.get_user(user_id, False)
    assert body.status_code == 404
