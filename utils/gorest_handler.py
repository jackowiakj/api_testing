import requests



class GoRESTHandler:

    base_url = 'https://gorest.co.in/public/v2'
    users_endpoint = '/users'


    headers = {
        'Authorization': 'Bearer 967963a283730f9cb659aa9aed616c2cb3ba857b48e78627258c4c7bae1c91c3'
    }
    def create_user(self, user_data):
        response = requests.post(self.base_url+self.users_endpoint, json=user_data, headers=self.headers)
        assert response.status_code == 201
        return response

    def get_user(self, user_id, existing=True):
        response = requests.get(f'{self.base_url}{self.users_endpoint}/{user_id}', headers=self.headers)
        if existing:
            assert response.status_code == 200
        else:
            response.status_code == 404
        return response

    def update_user(self, user_id, user_new_data):
        response = requests.put(f'{self.base_url}{self.users_endpoint}/{user_id}', json=user_new_data, headers=self.headers)
        return response

    def delete_user(self, user_id):
        response = requests.delete(f'{self.base_url}{self.users_endpoint}/{user_id}', headers=self.headers)
        assert response.status_code == 204
        return response
